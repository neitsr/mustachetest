package com.x.mustachetest.component

import android.content.Context
import android.graphics.PorterDuff
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.x.mustachetest.R
import com.x.mustachetest.extension.toPx
import kotlinx.android.synthetic.main.component_number_picker.view.*
import java.util.ArrayList


class NumberPickerComponent @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyle, defStyleRes) {

    interface OnSelectListener {
        fun select(value: Int)
    }
    private var onSelectListener: OnSelectListener? = null

    private var textViewWidth = 0
    private var textViews: ArrayList<TextView>

    private var min = 0
    private var max = 0
    private var total = 0
    private val minDefault = 0
    private val maxDefault = 8
    private val padding = 2f.toPx

    private var activeNumber = 0

    private var activeTextView = TextView(context)
    private var inactiveTextView = TextView(context)

    init {
        inflate(context, R.layout.component_number_picker, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.NumberPickerComponent)
        min = attributes.getInteger(R.styleable.NumberPickerComponent_NumberPicker_min, minDefault)
        max = attributes.getInteger(R.styleable.NumberPickerComponent_NumberPicker_max, maxDefault)

        attributes.recycle()

        total = max - min
        textViews = ArrayList(total)

        setupUI()
        setupListeners()
    }

    private fun setupUI() {
        textViews = ArrayList(total)

        var textView: TextView
        val typeface = resources.getFont(R.font.open_sans)
        for (i in min..max) {
            textView = TextView(context)
            textView.setTextAppearance(R.style.TextView_NumberInactive)
            textView.layoutParams = LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT)
            textView.gravity = Gravity.CENTER_VERTICAL
            textView.typeface = typeface
            textView.text = "${i}"

            textViews.add(textView)

            vNumbers.addView(textView)
        }

        vKnob.background.setColorFilter(ContextCompat.getColor(context, R.color.knob), PorterDuff.Mode.SRC)
    }

    private fun setupListeners() {
        viewTreeObserver
            .addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)

                    var gutter = (vNumbers.width/total*1f) - 50.toPx/total
                    textViews.forEachIndexed { i, tv ->
                        tv.x = 20.toPx + i*gutter
                    }
                    textViewWidth = gutter.toInt()

                    colorize()
                    snap(0L)
                }
            })

        vKnob.setOnClickListener { }

        vKnob.setOnTouchListener { v, motionEvent ->
            var newX: Float
            when (motionEvent.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_DOWN -> {
                    open()
                }
                MotionEvent.ACTION_MOVE -> {
                    newX = motionEvent.rawX - v.width/2
                    if (newX <= padding) {
                        newX = padding
                    }
                    else if(newX >= width - v.width - padding) {
                        newX = width - v.width.toFloat() - padding
                    }

                    v.translationX = newX
                    vBubbleX.translationX = newX

                    setActive()
                    colorize()
                }
                MotionEvent.ACTION_UP -> {
                    setActive()
                    colorize()
                    snap()
                    close()

                    onSelectListener!!.select(min + activeNumber)
                }
            }
            false
        }
    }

    fun setOnSelectListener(listener: OnSelectListener) {
        this.onSelectListener = listener
    }

    private fun setActive() {
        val x = vKnob.x + resources.getDimensionPixelSize(R.dimen.knob_size)/2
        activeNumber = (x/textViewWidth).toInt()
    }

    private fun colorize() {
        if (activeTextView != textViews[activeNumber]) {
            tvBubble.text = "${activeNumber + 1}"
            activeTextView = textViews[activeNumber]
            activeTextView.setTextAppearance(R.style.TextView_NumberActive)
            inactiveTextView.setTextAppearance(R.style.TextView_NumberInactive)
            inactiveTextView = activeTextView
        }
    }

    private fun snap(duration: Long = 200L) {
        var snapX = (textViewWidth*activeNumber + padding)
        var knobSize = 48.toPx
        if (snapX >= vNumbers.width - knobSize) {
            snapX = vNumbers.width.toFloat() - knobSize
        }
        vKnob.animate().translationX(snapX).duration = duration
        vBubbleX.animate().translationX(snapX).duration = duration
    }

    private fun open() {
        vBubbleY.animate().translationY(0f).duration = 200L
    }

    private fun close() {
        vBubbleY.animate().translationY(resources.getDimensionPixelSize(R.dimen.knob_size).toFloat()).duration = 200L
    }

    // Saved state
    public override fun onSaveInstanceState(): Parcelable? {
        val savedState = SavedState(super.onSaveInstanceState())
        savedState.activeNumber = activeNumber
        return savedState
    }

    public override fun onRestoreInstanceState(state: Parcelable) {
        if (state is SavedState) {
            super.onRestoreInstanceState(state.superState)
            activeNumber = state.activeNumber
        } else {
            super.onRestoreInstanceState(state)
        }
    }

    internal class SavedState: BaseSavedState {

        var activeNumber = 0

        constructor(source: Parcel) : super(source) {
            activeNumber = source.readInt()
        }

        constructor(superState: Parcelable) : super(superState)

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeInt(activeNumber)
        }

        @JvmField
        val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {

            override fun createFromParcel(source: Parcel): SavedState {
                return SavedState(source)
            }

            override fun newArray(size: Int): Array<SavedState?> {
                return arrayOfNulls(size)
            }
        }
    }
}